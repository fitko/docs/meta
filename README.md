# Dokumentation zur Nutzung des Föderalen Entwicklungsportals

## Lokale Entwicklung
To preview your changes as you edit the files, you can run a local development server that will serve your website and reflect the latest changes.

```shell
$ yarn
$ yarn start
```

See https://docusaurus.io/docs/installation#running-the-development-server for details.


# Lokale Apache-Testumgebung für Docusaurus

Diese Docker-Konfiguration ermöglicht das lokale Testen von Docusaurus-Builds inklusive .htaccess-Regeln.

## Voraussetzungen

- Node.js
- Optional: Yarn Package manager (npm kann auch verwendet werden)
- Docker & Docker Compose (beides in Docker Desktop enthalten)
- Docusaurus Build im `build/` Verzeichnis
- .htaccess-Datei im `static/` Verzeichnis

## Schnellstart

1. Build der Docusaurus-Seite erstellen:
```bash
$ yarn install
$ yarn build
```

2. Docker-Container starten:
```bash
$ docker-compose up --build
```

Die Seite ist dann unter `http://localhost:8090` erreichbar.

## Projektstruktur

```
.
├── build/             # Docusaurus Build-Output
├── static/
│   └── .htaccess      # Apache-Konfiguration
├── docker-compose.yml
├── Dockerfile
└── my-httpd.conf      # Apache-Hauptkonfiguration
```

## Entwicklung

- Änderungen an der `.htaccess` und am am Build-Output werden durch das Volume-Mounting sofort wirksam

## Steuerung

```bash
# Container im Vordergrund starten
docker-compose up --build

# Container im Hintergrund starten
docker-compose up -d --build

# Container stoppen
docker-compose down
```
