# Aufnahmekriterien 

Gemäß [Beschluss 2020/44](https://www.it-planungsrat.de/beschluss/beschluss-2020-44) des IT-Planungsrates soll das föderale Entwicklungsportal für alle relevanten Produkte und föderalen Basiskomponenten die benötigten technischen Dokumentationen, Entwicklungsressourcen und Leitfäden bedarfsorientiert bündeln und offen bereitstellen.

Ressourcen, die im Föderalen Entwicklungsportal [veröffentlicht werden](https://docs.fitko.de/resources/), müssen eine Reihe von Aufnahmekriterien erfüllen.
Diese gemeinsamen Mindeststandards an Qualität und Offenheit von Entwicklungsressourcen dienen der Gewährleistung eines einheitlichen, hohen Qualitätsniveaus der bereitgestellten Artefakte. Die Kriterien können daher auch als allgemeine Checkliste bei der Entwicklung von Software, APIs und Dokumentation verstanden werden.

:::info Aktuelle Version
Derzeit liegen die Aufnahmekriterien in der Version `0.3.0` vor.
:::

### Allgemeine Kriterien
- A.1: Die Ressource weist einen Bezug zu den [Produkten und Standards des IT-Planungsrat](https://www.it-planungsrat.de/produkte-standards), zu [Building Blocks der EU-Kommission](https://ec.europa.eu/digital-building-blocks/) oder zu offiziellen Basisdiensten der öffentlichen Verwaltung auf oder stellt eine nützliche Ressource zur effizienten Digitalisierung von Verwaltungsleistungen dar.
- A.2: Die Ressource erzeugt keine Lock-in-Effekte zu einzelnen Anbietern. Dies ist z.B. dann der Fall, wenn die Ressource unter einer freien Softwarelizenz (Open-Source-Lizenz) bereitsteht und keine Abhängigkeiten zu konkreten, herstellerspezifischen Produkten aufweist.

### Kriterien für Dokumentation / Textressourcen {#docs}
- D.1: Die Dokumentation liegt als verlinkbare Webseite vor. Insbesondere liegt die Dokumentation nicht ausschließlich als PDF vor. Für einfache Softwareartefakte kann dabei eine im Code-Repository enthaltene Dokumentation in Form von Markdown- oder AsciiDoc-Dateien ausreichend sein.

### Kriterien für Softwareartefakte {#software}
- S.1: Die Lizenz der Software genügt der [Freie-Software-Definition der FSF](https://www.gnu.org/philosophy/free-sw.de.html) sowie der [Open-Source-Definition der OSI](https://opensource.org/osd/]) und wird von beiden Organisationen als Freie-Software-Lizenz anerkannt. Eine gute Wahl stellt beispielsweise die von der Europäischen Kommission entwickelte [EUPL-Lizenz](https://joinup.ec.europa.eu/node/702120) dar.
- S.2: Der Quellcode der Software wird in einer Git-basierten Versionsverwaltung auf einer öffentlich zugänglichen Entwicklungsplattform bereitgestellt.
- S.3: Die genutzte Entwicklungsplattform (z.B. [OpenCoDE](https://opencode.de/)) verfügt über eine freie Registrierung und einen Feedback-Kanal inkl. der Möglichkeit zur Einreichung von Merge-Requests und einem Issue-Tracker.
- S.4: Die Software wird gemäß [Semantic-Versioning](https://semver.org/) versioniert.
- S.5: Es existiert Dokumentation zur Nutzung des Softwareartefakts, die den [Kriterien für Dokumentation](#docs) entsprechen.

### Kriterien für API-Spezifikationen {#APIs}
- P.1: Die Spezifikation liegt in einem maschinenlesbaren Format vor, z.B. [OpenAPI](https://www.openapis.org/).
- P.2: Beim Einsatz von OpenAPI wird die OpenAPI-Version `3.0.0` oder höher verwendet.
- P.3: Die Spezifikation wird gemäß [Semantic-Versioning](https://semver.org/) versioniert.

Die Aufnahmekriterien werden fortlaufend aktualisiert.
