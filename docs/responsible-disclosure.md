# Richtlinien zur verantwortungsvollen Offenlegung von Schwachstellen

Bei der FITKO legen wir sehr viel Wert auf die Sicherheit der von uns betrieben Systeme. 
Niemand ist perfekt und so kann es trotzdem passieren, dass eine Schwachstelle in einem der von der FITKO betriebenen Projekte und Produkte entdeckt wird.
Wir würden uns freuen, wenn ihr uns über gefundene Schwachstellen so schnell wie möglich nach dem „Responsible Disclosure“-Prinzip informiert, damit wir zeitnah Maßnahmen zum Schutz der von uns betriebenen IT-Systeme der öffentlichen Verwaltung und aller Nutzer:innen einleiten können.

Im Folgenden soll daher unser Umgang mit Schwachstellen im Detail beschrieben werden.
Dazu gehört auch, zu erklären, wie ihr uns Schwachstellen melden könnt und wie wir dann im Anschluss mit den von euch gemeldeten Schwachstellen umgehen.

Alternativ findet ihr die wesentlichen Informationen in Kurzform nach dem [Security.txt-Standard](https://securitytxt.org/) in der Datei [security.txt](https://docs.fitko.de/.well-known/security.txt).

## Meldung von Schwachstellen

Wenn ihr glaubt, eine Schwachstelle in einem von der FITKO bzw. im Auftrag der FITKO betriebenen Dienst gefunden zu haben, teilt uns dies bitte so schnell wie möglich nach Entdeckung mit, vorzugsweise per E-Mail an `it-sicherheit`@`fitko.de`.
Verwendet dabei bitte [unseren PGP-Schlüssel](https://keys.openpgp.org/search?q=924BDA2DF08D50B179CE3A46E4EDC7C651502C53) mit dem Fingerprint `924B DA2D F08D 50B1 79CE  3A46 E4ED C7C6 5150 2C53` sowie den **Betreff** `Responsible Disclosure: <Betroffener Dienst> <ggf. Art der Schwachstelle>`.
Wenn ihr einen telefonischen Austausch wünscht, gebt bitte eine Telefonnummer an, unter der wir euch erreichen können.

Folgende Details sind dabei für uns besonders relevant und helfen dabei, Lücken schneller zu beheben:
- eine Beschreibung, wo sich die Sicherheitslücke befindet und welche potenziellen Auswirkungen sie hat;
- eine detaillierte Beschreibung der Schritte, die erforderlich sind, um die Schwachstelle zu reproduzieren (verwendeter Browser und Version, Skripte, Zeitpunkt der Beobachtung, Screenshots, etc.);
- Eure Kontaktdaten (E-Mail-Adresse oder Telefonnummer), damit wir zum Fortschritt bei der Beseitigung der Schwachstelle oder für Rückfragen Kontakt aufnehmen können (wir nehmen aber auch anonyme Meldungen ernst).

Alternativ erreicht ihr den IT-Sicherheitsbeauftragten der FITKO, Marco Holz, unter der Telefonnummer +49 (69) 401270 139.
Weitere Kontaktdaten findet ihr [auf unserer Webseite](https://www.fitko.de/ueber-uns/wer-wir-sind).

### Rahmenbedingungen für einen verantwortungsvollen Umgang

Beim Umgang mit Schwachstellen sollten einige wichtige Verhaltensregeln beachtet werden:
1. Teilt eure Informationen über das Sicherheitsproblem nicht mit Dritten, bis das Problem gelöst ist und gebt uns damit bitte die Chance, das Problem zu beheben, bevor es zu größeren Schäden kommen kann.
2. Geht mit eurem Wissen über das Sicherheitsproblem verantwortungsbewusst um, d.h., tut nur das, was notwendig ist, um das Sicherheitsproblem kenntlich zu machen.
3. Nutzt die Schwachstelle nicht aus und speichert keine vertraulichen Daten, auf die ihr im Rahmen der Erforschung der Schwachstelle Zugriff erlangt habt.
4. Physische Angriffe, DDOS-Angriffe und Social Engineering tolerieren wir nicht. Wir sind aber für Hinweise dankbar, wenn ihr konkrete Anhaltspunkte dafür habt, dass solche Angriffe erfolgsversprechend sein könnten.
5. Verletzungen der Privatsphäre, Beeinträchtigungen der Nutzererfahrung, Störungen von Produktionssystemen sowie die Löschung von Daten müssen vermieden werden.

Unsere Richtlinien zur verantwortungsvollen Offenlegung stellen keine Aufforderung dar, von der FITKO bzw. im Auftrag der FITKO betriebenen Dienste oder zugehörige Netzwerke umfassend aktiv auf Schwachstellen zu überprüfen.
Wir überwachen unser Netzwerk und unsere Dienste zusammen mit unseren Partnern selbst.

### Wie verfährt die FITKO bei einer verantwortungsvollen Offenlegung?

Wenn eine mutmaßliche Schwachstelle in einem unserer IT-Systeme gemeldet wird, behandeln wir diese Meldung wie folgt:
- Innerhalb von drei Werktagen nach der Meldung bekommt ihr eine Empfangsbestätigung.
- Innerhalb von drei Werktagen nach der Empfangsbestätigung erhaltet ihr eine Reaktion, die eine Beurteilung der Meldung und das voraussichtliche Datum der Lösung beinhaltet.

### Weiterführende Informationen
- Empfehlung für IT-Hersteller „[Handhabung von Schwachstellen](https://www.allianz-fuer-cybersicherheit.de/SharedDocs/Downloads/Webs/ACS/DE/BSI-CS/BSI-CS_019.pdf?__blob=publicationFile&v=1)“ des BSI
- Blogbeitrag „[Wie gehe ich mit einer Sicherheitslücke um, die ich entdeckt habe?](https://jugendhackt.org/blog/wie-gehe-ich-mit-einer-sicherheitsluecke-um-die-ich-entdeckt-habe/)“ auf jugendhackt.org
- Vortrag „[Deine Software, die Sicherheitslücken und ich](https://media.ccc.de/v/rc3-2021-xhain-278-deine-software-die-si)“ auf media.ccc.de