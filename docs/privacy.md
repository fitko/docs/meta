# Datenschutzerklärung

Bitte beachten Sie die nachfolgenden Hinweise zur Verarbeitung Ihrer Daten gemäß Art. 13 der Europäischen Datenschutz-Grundverordnung (DSGVO).

Verweise auf gesetzliche Vorschriften beziehen sich auf die Europäische Datenschutz-Grundverordnung (DSGVO) sowie das Bundesdatenschutzgesetz (BDSG), das Hessische Datenschutz- und Informationsfreiheitsgesetz (HDSIG) und der Neufassung des Vertrags über die Errichtung des IT-Planungsrats und über die Grundlagen der Zusammenarbeit beim Einsatz der Informationstechnologie in den Verwaltungen von Bund und Ländern – Vertrag zur Ausführung von Art. 91c GG vom 13. Dezember 2019 (IT-Staatsvertrag).

## Geltungsbereich

Diese Datenschutzerklärung gilt für die von der FITKO (Föderale IT-Kooperation) AöR unter der Domain docs.fitko.de bereitgestellte Website des Föderalen Entwicklungsportals (inkl. aller Unterseiten). Für Internetseiten anderer Anbieter, auf die z. B. über Links verwiesen wird, oder die Dienste oder Inhalte dieser Seiten verfügbar machen, gelten die dortigen Datenschutzhinweise und -erklärungen.

## Name und Kontaktdaten des für die Verarbeitung Verantwortlichen

Verantwortlich für die Verarbeitung Ihrer personenbezogenen Daten ist die

FITKO (Föderale IT-Kooperation)\
Zum Gottschalkhof 3\
60594 Frankfurt am Main\
E-Mail: poststelle@fitko.de

Die FITKO ist eine Anstalt des öffentlichen Rechts. Sie wird vertreten durch den Präsidenten Dr. André Göbel.

## Behördliche:r Datenschutzbeauftragte:r

Die:Den Datenschutzbeauftragte:n der FITKO erreichen Sie per E-Mail unter datenschutz@fitko.de.

## Datenerfassung und -speicherung

### Webanalyse

Diese Website nutzt den Open-Source-Dienst Plausible Analytics, um die Nutzung unserer Website zu analysieren. Auf Grundlage des Art. 6 Abs. 1 lit. e DSGVO i. V. m. § 3 HDSIG wertet die FITKO im Rahmen der Öffentlichkeitsarbeit und zur bedarfsorientierten Bereitstellung von Informationen zu den wahrzunehmenden Aufgaben Nutzungsinformationen zu statistischen Zwecken aus.

Werden Einzelseiten unserer Website aufgerufen, so werden folgende Daten gespeichert:

* die aufgerufene Website (URL),
* die Website, von der die Nutzenden auf die aufgerufene Website gelangt sind (Referrer),
* Browser,
* Betriebssystem,
* Gerätetyp,
* Staat,
* Region und
* Stadt.

Informationen zum Datenschutz bei Plausible Analytics finden Sie unter:\
https://plausible.io/data-policy

Anonymisierte, statistische Webanalysedaten können Sie selbst einsehen unter:\
https://plausible.io/docs.fitko.de

## Kontakt mit der FITKO

Wenn Sie mit der FITKO Kontakt aufnehmen, sei es telefonisch, per E-Mail, schriftlich, durch Teilnahmen an (Online-)Veranstaltungen, Meetings oder auf anderem Wege, verarbeiten wir die personenbezogenen Daten, die Sie uns in diesem Zusammenhang mitteilen. In der Regel handelt es sich dabei um dienstliche Kontaktdaten. Die Rechtsgrundlage der Verarbeitung für allgemeine Kontakte mit der FITKO ist grundsätzlich § 3 Abs. 1 HDSIG (Erfüllung gesetzlicher Aufgaben). Eine Weitergabe dieser Daten, etwa an die für die Erfüllung der Aufgaben aus dem IT-Staatsvertrag zuständigen Behörden des Bundes, der Länder oder von diesen beauftragten Behörden oder Dienstleister:innen kann grundsätzlich dann erfolgen, wenn dies erforderlich ist um z. B. Ihre Anfrage zu bearbeiten oder an die zuständige Stelle, etwa die Projektleitung, weiterzuleiten. Wenn Sie dies nicht wünschen, können Sie der Weitergabe im Rahmen Ihrer Kontaktaufnahme widersprechen. Im Sinne der Datensparsamkeit möchten wir Sie bitten, uns nur die erforderlichen personenbezogenen Daten mitzuteilen und grundsätzlich auf die Übermittlung besonderer Kategorien personenbezogener Daten gemäß Art. 9 DSGVO zu verzichten. Die Datenverarbeitung erfolgt zum Zweck der Bearbeitung Ihres Anliegens und/ oder der Erfüllung der jeweiligen Aufgabe (Management des jeweiligen Projektes, Betrieb des jeweiligen Produktes, Vernetzung der Akteure der Verwaltungsdigitalisierung und Unterstützung der Arbeit des IT-Planungsrates).

## Aufbewahrungs- und Löschfristen

Die Löschung der Daten erfolgt, wenn ihre Speicherung nicht mehr erforderlich ist und etwaige Aufbewahrungspflichten, etwa von Rechnungsunterlagen, Verträgen und Projektdokumentationen, abgelaufen sind. Die allgemeine Aufbewahrungsfrist beträgt nach dem Erlass zur Aktenführung in den Dienststellen des Landes Hessen (Aktenführungserlass – AfE) vom 14. Dezember 2012 in der Regel 5 Jahre. Besondere Löschfristen sind ggf. in den jeweiligen Abschnitten aufgeführt.

## Ihre Rechte

Sie haben gegenüber der FITKO folgende Rechte hinsichtlich der Sie betreffenden personenbezogenen Daten:

* Recht auf Auskunft, Art. 15 DSGVO:

  Sie können Auskunft über Ihre von uns verarbeiteten personenbezogenen Daten verlangen. Wir möchten Sie bitten, Ihre Anfrage konkret und präzise zu formulieren.
* Recht auf Berichtigung, Art. 16 DSGVO

  Sie haben das Recht, Ihre personenbezogene Daten berichtigen zu lassen. Sollten Ihre Daten unvollständig sein, können Sie eine Vervollständigung verlangen.
* Recht auf Löschung, Art. 17 DSGVO

  Sie können die Löschung Ihrer personenbezogenen Daten verlangen. Ihr Anspruch auf Löschung hängt auch davon ab, ob die Daten noch zur Erfüllung unserer gesetzlichen Aufgaben benötigt werden.
* Recht auf Einschränkung der Verarbeitung, Art. 18 DSGVO

  Sie können die Einschränkung der Verarbeitung Ihrer personenbezogenen Daten nach Maßgabe der gesetzlichen Regelungen verlangen. Dies ist insbesondere dann relevant, wenn die Prüfung für das Recht auf Löschung oder Berichtigung noch nicht abgeschlossen ist.
* Recht auf Widerspruch gegen die Verarbeitung, Art. 21 DSGVO

  Sie haben das Recht, der Verarbeitung Ihrer personenbezogenen Daten zu widersprechen, was insbesondere bei Profiling und Direktwerbung relevant ist.
* Recht auf Datenübertragbarkeit, Art. 20 DSGVO

  Sie haben das Recht auf Übertragung Ihrer personenbezogenen Daten unter den dort genannten Einschränkungen.
* Recht auf Widerruf der Einwilligung, Art. 7 DSGVO

  Soweit die Verarbeitung der personenbezogenen Daten auf Grundlage einer Einwilligung erfolgt, können Sie diese jederzeit für den entsprechenden Zweck widerrufen. Die Rechtmäßigkeit der Verarbeitung aufgrund der getätigten Einwilligung bleibt bis zum Eingang des Widerrufs unberührt.

Die vorgenannten Rechte können Sie bei der

FITKO (Föderale IT-Kooperation)\
Zum Gottschalkhof 3\
60594 Frankfurt am Main\
E-Mail: datenschutz@fitko.de

geltend machen.

Wenn Sie der Auffassung sind, dass wir bei der Verarbeitung Ihrer Daten datenschutzrechtliche Vorschriften nicht beachtet haben, können Sie sich mit einer Beschwerde gemäß Art. 77 DSGVO an die zuständige Aufsichtsbehörde wenden:

Der Hessische Beauftragte für Datenschutz und Informationsfreiheit\
Gustav-Stresemann-Ring 1\
65189 Wiesbaden\
https://datenschutz.hessen.de