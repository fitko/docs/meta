# Deployment
This page explains how deployment works for docusaurus subpages (like this one itself with project slug `/meta/`).

Deployment is triggered via GitLab webhooks when one of the following CI stages are entered:
- `upload:preview` -> deployment to `preview.docs.fitko.dev/<PROJECT_SLUG>/<MERGE_REQUEST_ID>/`
- `stop:preview` -> preview deployment is deleted again
- `upload:production` -> deployment to `docs.fitko.de/<PROJECT_SLUG>/`

When configuring a new docusaurus instance for deployment, your Gitlab repo needs to be configured properly and your project needs to be registered in the deployment webserver. Please [contact FITKO](./intro.md#kontakt) for registering any new projects.

## Configure your Gitlab project {#gitlab}
1. Use [`.gitlab-ci.yml` from the template repo](https://gitlab.opencode.de/fitko/docs/docusaurus-template) as your CI template.
1. The deployment assumes there is a 'build/' folder in the `build` jobs artifact. Make sure not to modify the `.gitlab-ci.yml` template here.
1. Configure a webhook in your project or group:
   1. Go to `Settings` -> `Webhooks`
   1. Use `https://docs.fitko.de/hooks/deploy` as the webhook url
   1. Ask for a webhook secret and use it as the secret token
   1. Check `Job events` as trigger
   1. Click `Add webhook`

## Register your repository at docs.fitko.de (webserver) {#webserver}
To register your project, we only need the following info:
1. Your gitlab project id
1. Your prefered `<PROJECT_SLUG>`

## Behind the scenes
We use [`adnanh/webhook`](https://github.com/adnanh/webhook/) for running webhooks.
