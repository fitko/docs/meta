---
id: usage
title: Anleitung zur Inhaltsbearbeitung 
---

# Anleitung zur Inhaltsbearbeitung

:::info Zusammenfassung
Hier finden Sie Hilfestellungen Rund um den Umgang mit GitLab, Merge Requests und Markdown im Föderalen Entwicklungsportal.
:::

## Einführung

### Git-Repository mit GitLab

Alle Inhalte für einen Dokumentationsbereich im föderalen Entwicklungsportal werden in einem Git-Repository abgelegt, welches durch eine GitLab-Instanz bereitgestellt wird.

### Rendering mit Docusaurus

Die üblicherweise in der Markdown-Sprache beschriebenen Inhalte eines Dokumentationsprojekts werden mit dem Open-Source-Tool [Docusaurus](<https://docusaurus.io/>) zu einem statischen Set an HTML-Dateien übersetzt.
Diese werden dann auf einen Webserver übertragen.
Nach dem Abschluss der [Deployment-Konfiuration](./deploy.md) läuft dieser Übersetzungsprozess vollautomatisch ab.
Die Ablage der HTML-Dateien erfolgt auf einem von der FITKO bereitgestellten Webserver.

### Versionsverwaltung mit GitLab

Die Nutzung von GitLab bringt mit neben der Git-üblichen Versionsverwaltung einige weitere Features zum gemeinsamen Arbeiten an den Inhalten mit.
Diese werden neben weiteren Bearbeitungsvorgaben und Empfehlungen im weiteren Verlauf dieses Dokuments beschrieben.

## Ablagestruktur

### Dokumente

Alle Inhaltsdokumente werden im Ordner `docs` des Repository-Root-Verzeichnisses abgelegt. Die Strukturierung (Anzahl und Namen der Dateien, sowie Unterverzeichnisse) innerhalb des `docs`-Ordners kann wiederrum nach konkreten Bedarfen angelegt werden.

:::info tip
Die Darstellung und Struktur eines Navigationsbaums hängt nicht zwangsläufig von der Ablagestruktur ab, sondern kann auch separat konfiguriert werden.
:::

### Bilder

Bilder können im Unterordner `static/images` abgelegt werden.
Auch hier ist nach Bedarf die Erstellung von Unterverzeichnissen möglich.

### Dateinamen

Dateinamen sollten sprechend sein und das behandelte Thema oder dargestellte Bild mit 1-2 Worten benennen. Zur besseren Sortierung können Ordner verwendet werden.

Statt Leerzeichen sind Bindestriche zu verwenden, falls eine Trennung erforderlich ist.

Beispiele:

- `Einleitung.md`
- `Anleitung-zur-Nutzung.md`

## Markdown-Dateien

### Header

In Markdown-Dateien kann ein Header enthalten sein, der beispielhaft wie folgt aussieht:

![Beispiel für den Header](/images/usage/header.png)

Dabei kann die ID des Markdown-Dokuments mit dem Attribut `id` gesetzt werden.
Der Standardwert der ID entspricht dem Dateinamen ohne Dateiendung (Beispiel: Die standardmäßige ID des Dokuments `Einleitung.md` lautet `Einleitung` ).
Mit dem Attribut `title` kann der Titel des Dokuments überschrieben werden.
Standardwert für den Titel eines Dokuments ist die Bezeichnung der ersten Kapitelüberschrift.

Weitere Informationen hierzu finden sich in der Dokumentation von Docusaurus (en):

- [Erstellung von Dokumenten](https://docusaurus.io/docs/create-doc)
- [Beschreibung der möglichen Attribute im Header](https://docusaurus.io/docs/api/plugins/@docusaurus/plugin-content-docs#markdown-frontmatter)

### Markdown-Syntax

Markdown ist eine leichtgewichtige Beschreibungssprache, um einfache Texte mit Formatierungselementen zu ergänzen.
Docusaurus unterstützt neben den Standard-Elementen auch verschiedene Erweiterungen.

Weiterführende Guides:

- [Grundlagen zur Markdown-Syntax](https://www.markdownguide.org/basic-syntax/)
- [Weitere Docusaurus-spezifische Markdown-Features](https://docusaurus.io/docs/markdown-features)

### Dokument-Referenzierungen

Dokumente können auch untereinander verlinkt werden, wie folgendes Beispiel zeigt:

![Referenzierung von Dokumenten](/images/usage/dokument-referenzierungen.png)

Weitere Infos:

- [Referenzieren von Dokumenten](https://docusaurus.io/docs/docs-markdown-features#referencing-other-documents)

## Arbeiten mit GitLab

### WebIDE

Die Dokumente lassen sich am einfachsten in der GitLab WebIDE bearbeiten, ohne das Git-Repository auf den eigenen Rechner klonen zu müssen.
Für die Erstellung oder Bearbeitung von Ordernstrukturen und Dateien wird in der Bearbeitungs-Ansicht gearbeitet:

![Bearbeitungsansicht](/images/usage/webIDE.png)

### Branches und Merge Requests

Änderungen sollten thematisch ausschließlich in Branches und Merge Requests durchgeführt werden.
Ein Branch ist ein Feature von Git.
Hierbei wird ein Arbeitsbereich als Nebenzweig vom sogenannten `main`-Branch temporär abgespaltet, um diesen später - nach Durchführung aller Änderungen - in den Hauptzweig, dem `main`-Branch, wieder einzugliedern (*mergen*).
Um hier noch eine zusätzliche Qualitätssicherungsebene zu erhalten, wird ein Branch von einem GitLab Merge Request eingeklammert.
Dieser bietet mehrere Funktionen, um ein Review der Änderungen nach dem Vier- oder Mehr-Augen-Prinzip durchzuführen, bevor die Änderungen im Branch des Nebenzweigs in den Hauptzweig überführt wird.

:::caution Wichtig
Alle Inhalte im Hauptzweig (`main`-Branch) werden automtisch im öffentlichen Dokumentationsbereich des Entwicklungsportals dargestellt.
Insofern ist es aus Qualitätsgründen sehr zu empfehlen, eine Vorab-Prüfung aller Inhalte im Hauptzweig sicherzustellen.
Dazu eignen sich bestens die Merge-Request-Funktionalitäten von GitLab.
:::

### Änderungen in neuen Branch und Merge Request

Wenn Änderungen durchgeführt wurden, sind diese per Commit einzuchecken.
Die geplanten Änderungen müssen zu diesem Zeitpunkt noch nicht abgeschlossen sein.
Es können nachfolgend noch weitere Änderungen vorgenommen werden.

Dazu ist über den entsprechenden Button in der linken, seitlichen Menü-Leiste in die Commit-Ansicht zu wechseln:

![Commit-Ansicht](/images/usage/commit-ansicht.png)

In dieser Ansicht können alle Änderungen noch geprüft werden.
Außerdem ist eine **Commit message** zu erstellen – also eine kurze Zusammenfassung was geändert bzw. ergänzt wurde.

Durch Betätigen der **Commit & Push**-Schaltfläche öffnet sich ein Dialog mit der Frage, ob ein neuer Branch erstellt werden werden soll oder der aktuelle Branch weitergenutzt wird.

![Commit Branch](/images/usage/new-branch.png)

Wird ein neuer Branch erstellt, kann ggf. der vorgeschlagenen Default-Name durch einen sprechenderen ersetzt werden.

![New Branch Name](/images/usage/new-branch-name.png)

Nach erfolgreichem Commit kann direkt über **Create MR** ein neuer Merge Request angelegt werden, welcher im Anschluss noch bearbeitet werden kann.

![Erfolgreicher Commit](/images/usage/successful-commit.png)

Folgende Felder sollten im Merge Request geprüft und angepasst werden:

- **Title** – Bitte einen sprechenden Titel vergeben und solange der Änderungsprozess noch nicht abgeschlossen ist ein Draft: vorne dranstellen.
- **Description** – Verfassen einer detaillierteren Beschreibung, damit andere einen Überblick über die inhaltlichen Änderungen im Merge Request erhalten können.
- **Assignee** – Haupt-Bearbeiter:in des Merge Requests
- **Reviewer** – Haupt-Reviewer:in des Merge Requests – sobald diese feststeht

![Merge Request bearbeiten](/images/usage/edit-mr.png)

Alle anderen Einstellungen so belassen oder vornehmen, wie im Screenshot oben dargestellt.

### Änderungen in bestehendem Branch oder Merge Request durchführen

In der Bearbeitungsansicht den geeigneten Branch oder Merge Request auswählen – falls nicht schon bereits erfolgt.

![Branch wählen](/images/usage/chose-branch.png)

:::caution Achtung
Änderungen, die einem anderen Branch vorgenommen worden sind und noch nicht durch einen Commit eingecheckt wurden, gehen durch Wechsel zu einem anderen Branch bzw. Merge Request verloren.
:::

Dann die Ordnerstruktur bzw. Dateien bearbeiten und letztendlich in die Commit-Ansicht wechseln.

Hier die Änderungen nochmal prüfen und zum Commit die Schritte analaog zu [Änderungen in neuen Branch und Merge Request](#änderungen-in-neuen-branch-und-merge-request) durchführen.

### Review durchführen

Bevor Änderungen in den Hauptzweig übernommen werden, sind diese durch andere gegenzuprüfen.

Die Änderungen können in der Change-View des Merge Requests eingesehen werden.

![Change-View des Merge Requests](/images/usage/change-view-mr.png)

Für jede geänderte Datei wird ein Block angezeigt.
Jede Änderung kann auch direkt dort kommentiert an der jeweiligen Zeile kommentiert werden.
Ebenso können Änderungsvorschläge formuliert werden.

![Änderungsvorschläge](/images/usage/change-proposal.png)

Die Änderungsvorschläge können angenommen oder auch verworfen werden.

Ist das Review erfolgreich abgeschlossen (ggf. nach mehreren Überarbeitungs-Schleifen, dann kann die reviewende Person dies auf der Overview-Seite des Merge Requests bestätigen. (&rarr; Approve-Button)).

### Rendering-Ergebnis prüfen

Im Rahmen des Reviews ist auch oftmals sinnvoll das Rendering zu prüfen.
Dazu wird nach jedem Commit das Docusaurus-System angestoßen.
Dieses generiert für den jeweiligen Branch einen separaten nicht verlinkten Preview-Bereich.

Das Schema der Aufruf-Url dazu ist:  
```https://preview.docs.fitko.dev/<project-slug>/!<merge-request-id>```


Die Preview-Ansicht ist auch über den Button `View App` im Merge-Request erreichbar.

### Merge durchführen

Wenn der Merge Request noch im Draft-Modus ist, dann ist der Merge Request als Ready zu markieren.

![Merge](/images/usage/merge.png)

Der Merge-Prozess wird durch betätigen des Merge-Buttons gestartet.
Hierzu müssen die entsprechenden User-Berechtigungen vorliegen.

## Unterstützung erhalten

Du benötigst Unterstützung oder suchst einen Raum für gegenseitigen Austausch bei der Erstellung von Dokumentation, insb. mit Docusaurus? In folgendem Matrix-Channel ist Platz dafür:

| [#docs-community:matrix.org](https://matrix.to/#/#docs-community:matrix.org) |
| ----- |