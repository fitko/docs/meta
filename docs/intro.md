---
slug: /
---

# Überblick

Vielen Dank für Ihr Interesse an der Nutzung des Föderalen Entwicklungsportals.
Auf den folgenden Seiten beschreiben wir, wie das Föderale Entwicklungsportal zur Veröffentlichung von Dokumentation, API-Spezifikationen, Richtlinien und Implementierungsbeispielen genutzt werden kann.

:::info 🏗️
Die Dokumentation zur Nutzung des Föderalen Entwicklungsportals befindet sich derzeit noch im Aufbau.

Wir freuen uns jederzeit über Feedback im [dafür eingerichteten öffentlichen Issue-Tracker](https://docs.fitko.de/feedback).
:::

## Aufbau des Föderalen Entwicklungsportals

Die [Einstiegsseite des Föderalen Entwicklungsportals](https://docs.fitko.de/) bietet einen Überblick über die im Entwicklungsportal vorhandenen Ressourcen. Der Quelltext der Einstiegsseite wird [in einem öffentlichen Git-Repository](https://git.fitko.de/fit-connect/entwicklungsportal) verwaltet.

Unterhalb der Einstiegsseite können thematisch zusammenhängen Dokumentationsseiten auf Basis des Static-Site-Generator [Docusaurus](https://docusaurus.io/) erstellt werden. Docusaurs erzeugt [via GitLab-CI](https://about.gitlab.com/topics/ci-cd/) eine [statische Webseite](https://en.wikipedia.org/wiki/Static_web_page) aus einer Sammlung aus [Markdown](https://de.wikipedia.org/wiki/Markdown)-Files. Dokumentationsseiten werden dazu ebenfalls in einem Git-Repository verwaltet. Hier einige Beispiele für existierenden Dokumentationsseiten:
- Dokumentation zu FIT-Connect: [Dokumentationsseite im Entwicklungsportal](https://docs.fitko.de/fit-connect/) | [Git-Repository mit Markdown-Files](https://git.fitko.de/fit-connect/docs/)
- Dokumentation zu Standards und Schnittstellen: [Dokumentationsseite im Entwicklungsportal](https://docs.fitko.de/standards-und-schnittstellen/) | [Git-Repository mit Markdown-Files](https://git.fitko.de/ag-standards-und-schnittstellen/standards-und-schnittstellen)

## Kontakt

Bei Interesse an der Nutzung des Föderalen Entwicklungsportals, kontaktieren Sie bitte das Föderale IT-Architekturmanagement der FITKO via `architekturmanagement <@> fitko.de`.