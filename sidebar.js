module.exports = {
  defaultSidebar: [
    'intro',
    'acceptance-criteria',
    'usage',
    'deploy',
    'responsible-disclosure',
    'privacy'
  ]
}
